package dai.java.pattern.product.bag;

public class BagOrange implements IBag {
    private String desc_;

    public void setDesc(String desc) {
        desc_ = desc;
    }

    @Override
    public String desc() {
        return desc_;
    }

    public BagOrange() {
    }

    public BagOrange(String desc) {
        desc_ = desc;
    }
}
