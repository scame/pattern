package dai.java.pattern.product.bag;

public class BagApple implements IBag {
    private String desc_;

    public void setDesc(String desc) {
        desc_ = desc;
    }

    @Override
    public String desc() {
        return desc_;
    }

    public BagApple() {
    }

    public BagApple(String desc) {
        desc_ = desc;
    }
}
