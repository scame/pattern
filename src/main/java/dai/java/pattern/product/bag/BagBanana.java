package dai.java.pattern.product.bag;

public class BagBanana implements IBag {
    private String desc_;

    public void setDesc(String desc) {
        desc_ = desc;
    }

    @Override
    public String desc() {
        return desc_;
    }

    public BagBanana() {
    }

    public BagBanana(String desc) {
        desc_ = desc;
    }
}
