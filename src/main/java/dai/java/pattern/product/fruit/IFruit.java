package dai.java.pattern.product.fruit;

public interface IFruit {
    float price();

    String desc();
}
