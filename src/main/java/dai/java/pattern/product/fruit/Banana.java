package dai.java.pattern.product.fruit;

public class Banana implements IFruit {
    private float price_;
    private String desc_;

    public Banana() {
        this(0F, null);
    }

    public Banana(float price) {
        this(price, null);
    }

    public Banana(float price, String desc) {
        price_ = price;
        desc_ = desc;
    }

    public void setPrice(float price) {
        price_ = price;
    }

    @Override
    public float price() {
        return price_;
    }

    public void setDesc(String desc) {
        desc_ = desc;
    }

    @Override
    public String desc() {
        return desc_;
    }
}

