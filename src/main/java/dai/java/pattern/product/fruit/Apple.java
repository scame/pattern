package dai.java.pattern.product.fruit;

public class Apple implements IFruit {
    private float price_;
    private String desc_;

    public Apple() {
        this(0F, null);
    }

    public Apple(float price) {
        this(price, null);
    }

    public Apple(float price, String desc) {
        price_ = price;
        desc_ = desc;
    }

    public void setPrice(float price) {
        price_ = price;
    }

    @Override
    public float price() {
        return price_;
    }

    public void setDesc(String desc) {
        desc_ = desc;
    }

    @Override
    public String desc() {
        return desc_;
    }
}
