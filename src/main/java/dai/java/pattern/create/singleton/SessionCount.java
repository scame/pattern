package dai.java.pattern.create.singleton;

import java.util.concurrent.atomic.AtomicInteger;

public class SessionCount {
    private AtomicInteger count_ = new AtomicInteger(0);

    private static SessionCount instance_ = null;

    private SessionCount() {
    }

    // 饿汉模式
    //public static SessionCount getInstance() {
    //    return instance_;
    //}

    // 懒汉模式一(线程不安全)
    //public static SessionCount getInstance() {
    //    if (null == instance_) {
    //        instance_ = new SessionCount();
    //    }
    //    return instance_;
    //}

    // 懒汉模式二(线程安全, 但性能消耗高)
    //public static synchronized SessionCount getInstance() {
    //    if (null == instance_) {
    //        instance_ = new SessionCount();
    //    }
    //    return instance_;
    //}

    // 双重检查懒汉模式(线程安全, 性能高)
    public static SessionCount getInstance() {
        if (null == instance_) {
            synchronized (SessionCount.class) {
                if (instance_ == null) {
                    instance_ = new SessionCount();
                }
            }
        }
        return instance_;
    }

    public int plus() {
        return count_.incrementAndGet();
    }

    public int decrease() {
        return count_.decrementAndGet();
    }

    @Override
    public String toString() {
        return "current session number: " + count_.get();
    }
}
