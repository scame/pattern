package dai.java.pattern.create.singleton;

public class SessionCountClient {

    public static void main(String[] args) {
        SessionCount count = SessionCount.getInstance();

        new Thread(new Runnable() {
            @Override
            public void run() {
                count.plus();
                count.plus();
                count.plus();
                count.decrease();
                count.plus();
                count.plus();
                count.plus();
            }
        }).start();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        count.plus();
        count.plus();
        count.plus();
        count.decrease();

        System.out.println(count.toString());
    }
}
