package dai.java.pattern.create;

import dai.java.pattern.create.abstractFactory.AbstractFactory;
import dai.java.pattern.create.abstractFactory.Factory4Apple;

public class Main {
    public static void main(String[] args) {
        AbstractFactory factory = new Factory4Apple();
        System.out.println(factory.createFruit().desc());
    }
}
