package dai.java.pattern.create.abstractFactory;

import dai.java.pattern.product.bag.BagBanana;
import dai.java.pattern.product.bag.IBag;
import dai.java.pattern.product.fruit.Banana;
import dai.java.pattern.product.fruit.IFruit;

public class Factory4Banana extends AbstractFactory {

    @Override
    public IFruit createFruit() {
        return new Banana(3.6F, "Banana");
    }

    @Override
    public IBag createBag() {
        return new BagBanana("BagForBanana");
    }
}
