package dai.java.pattern.create.abstractFactory;

import dai.java.pattern.product.bag.BagOrange;
import dai.java.pattern.product.bag.IBag;
import dai.java.pattern.product.fruit.IFruit;
import dai.java.pattern.product.fruit.Orange;

public class Factory4Orange extends AbstractFactory {

    @Override
    public IFruit createFruit() {
        return new Orange(4.5F, "Orange");
    }

    @Override
    public IBag createBag() {
        return new BagOrange("BagForOrange");
    }
}
