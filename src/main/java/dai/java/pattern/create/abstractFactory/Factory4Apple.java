package dai.java.pattern.create.abstractFactory;

import dai.java.pattern.product.bag.BagApple;
import dai.java.pattern.product.bag.IBag;
import dai.java.pattern.product.fruit.Apple;
import dai.java.pattern.product.fruit.IFruit;

public class Factory4Apple extends AbstractFactory {

    @Override
    public IFruit createFruit() {
        Apple apple = new Apple(2.5F, "Apple");
        return apple;
    }

    @Override
    public IBag createBag() {
        BagApple bag = new BagApple();
        bag.setDesc("BagForApple");
        return bag;
    }
}
