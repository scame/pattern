package dai.java.pattern.create.abstractFactory;

import dai.java.pattern.product.bag.IBag;
import dai.java.pattern.product.fruit.IFruit;

public abstract class AbstractFactory {

    public abstract IFruit createFruit();

    public abstract IBag createBag();

}
