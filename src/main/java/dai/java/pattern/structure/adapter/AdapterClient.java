package dai.java.pattern.structure.adapter;

import dai.java.pattern.product.bag.BagApple;
import dai.java.pattern.product.bag.BagOrange;
import dai.java.pattern.product.fruit.Orange;

public class AdapterClient {

    public static void main(String[] args) {
        Orange orange = new Orange(5.6F, "Orange[5.6]");
        BagOrange bagOrange = getBagWithAppleBag();

        System.out.println(bagOrange.desc());
    }


    private static BagOrange getBag() {
        return new BagOrange("Orange_Bag");
    }

    private static BagOrange getBagWithAppleBag() {
        return new BagOrangeAdapter(new BagApple("OrangeBagWithApple"));
    }
}
