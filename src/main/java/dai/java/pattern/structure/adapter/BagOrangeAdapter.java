package dai.java.pattern.structure.adapter;

import dai.java.pattern.product.bag.BagApple;
import dai.java.pattern.product.bag.BagOrange;

public class BagOrangeAdapter extends BagOrange {

    private BagApple apple_;

    public BagOrangeAdapter(BagApple apple) {
        apple_ = apple;
    }

    @Override
    public String desc() {
        return apple_.desc();
    }
}
